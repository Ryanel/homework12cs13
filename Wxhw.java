/*
 * Wxhw - Use The OpenWeatherMap API To retrieve weather data from a zip code
 * Corwin McKnight
 * April 27, 2019
 */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import java.util.*; 
import com.google.gson.*;
import java.text.SimpleDateFormat;
class Wxhw {

   static final String API_KEY = "1191c93470ba92fd6ed8831a4bb5f6e8";
   
   // Helper functions
   private static double hPaToInHg(double hPa) {
        return hPa * 0.02953;
   }
   
   public static void main(String args[])
   {
        String zipCode = "95677";
        if(args.length == 0) {
            System.out.println("No arguments, using 95677 for testing");
        }
        else {
            zipCode = args[0]; // Ignore other args
        }

        // API Request
        JsonElement jse = null;
        
        try {
            // Construct URL
            String urlString = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipCode + "&APPID=" + API_KEY + "&units=imperial";
            System.out.println("GET " + urlString);
            URL apiRequest = new URL(urlString);
            
            // Make request and recieve data (sync)
            InputStream is = apiRequest.openStream(); // Can throw
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            jse = new JsonParser().parse(br);
            
            is.close();
            br.close();
        }
        catch(java.io.UnsupportedEncodingException uee) {
            uee.printStackTrace();
            return;
        }
        catch(java.io.FileNotFoundException ex) {
            System.out.println("ERROR: " + zipCode + " is an invalid query!");
            return;
        }
        catch (java.net.MalformedURLException mue) {
            System.out.println("Internal Error: Malformed URL!");
            return;
        }
        catch(Exception ex) {
            System.out.println("Internal Error: Unknown Exception!");
            ex.printStackTrace();
            return;
        }
        
        // We have some data, process it.
        if(jse == null) {
            System.out.println("Error: API returned invalid JSON data.");
            return;
        }
        // First, get code. 200 = good, anything else = bad. 
        String statusCode = jse.getAsJsonObject().get("cod").getAsString();
        
        if(statusCode.equals("200"))
        {
            JsonObject mainObj = jse.getAsJsonObject().getAsJsonObject("main");
            JsonObject windObj = jse.getAsJsonObject().getAsJsonObject("wind");
            JsonObject weatherObj = jse.getAsJsonObject().getAsJsonArray("weather").get(0).getAsJsonObject();
            
            String name = jse.getAsJsonObject().get("name").getAsString();
            String weatherType = weatherObj.get("main").getAsString();
            String currentTime = jse.getAsJsonObject().get("dt").getAsString();

            String temp = mainObj.get("temp").getAsString();
            String pressure = mainObj.get("pressure").getAsString();
            
            String windDeg = windObj.get("deg").getAsString();
            String windSpd = windObj.get("speed").getAsString();
            
            // Do any conversions...
            double hg = hPaToInHg(Double.parseDouble(pressure));
            
            Date date = new java.util.Date(Long.parseLong(currentTime) * 1000);
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM d, y 'at' h:mm a, zzzz");
            String formattedDate = sdf.format(date);
             
            // Print info out
            System.out.println("Where: " + name);
            System.out.println("Last updated:" + formattedDate);
            System.out.println("Weather: " + weatherType);
            System.out.println("Temp (F): " + temp );
            System.out.println("Wind: From " + windDeg + " degrees at " + windSpd +  " MPH");
            System.out.println("Pressure in inHG: " + Double.toString(hg));
            
        }
        else  {
            System.out.println("Status: " + statusCode + "message: " +  jse.getAsJsonObject().get("message").getAsString());
        }
        return;
    }
}